import lab01.tdd.*;
import lab01.tdd.factory.EqualFactory;
import lab01.tdd.factory.EvenFactory;
import lab01.tdd.factory.MultipleFactory;
import lab01.tdd.strategy.SelectStrategy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private CircularList integerList;
    @BeforeEach
    public void setUp(){
        integerList = new CircularListImpl();
    }
    @Test
    public void initiallySize(){
        assertEquals(integerList.size(), 0);
    }
    @Test
    public void initiallyEmpty(){
        assertEquals(integerList.isEmpty(),true);
    }
    @Test
    public void testAdd(){
        this.integerList.add(1);
        assertEquals(integerList.size(), 1);
    }

    @Test
    public void testNextElement(){
        this.integerList.add(1);
        this.integerList.add(2);
        assertEquals(integerList.next().get(), 2);
        assertEquals(integerList.next().get(), 1);
        this.integerList.add(3);
        assertEquals(integerList.next().get(), 2);
    }
    @Test
    public void testPreviousElement(){
        this.integerList.add(1);
        this.integerList.add(2);
        assertEquals(integerList.previous().get(), 2);
        this.integerList.add(3);
        assertEquals(integerList.previous().get(), 1);
    }
    @Test
    public void testReset(){
        this.integerList.add(1);
        this.integerList.add(2);
        this.integerList.add(3);
        this.integerList.reset();
        assertEquals(integerList.next().get(), 2);
    }


    @Test
    public void testNextEvenStrategy(){
        this.integerList.add(1);
        this.integerList.add(2);
        this.integerList.add(3);


        SelectStrategy st = new EvenFactory().getStrategy();
        assertEquals(integerList.next(st).get(), 2);
    }
    @Test
    public void testNextMultipleStrategy(){
        this.integerList.add(1);
        this.integerList.add(2);
        this.integerList.add(3);
        SelectStrategy st = new MultipleFactory(3).getStrategy();
        assertEquals(integerList.next(st).get(), 3);
    }
    @Test
    public void testNextEqualsStrategy(){
        this.integerList.add(1);
        this.integerList.add(2);
        this.integerList.add(3);

        SelectStrategy st = new EqualFactory(1).getStrategy();
        assertEquals(integerList.next(st).get(), 1);
    }
}
