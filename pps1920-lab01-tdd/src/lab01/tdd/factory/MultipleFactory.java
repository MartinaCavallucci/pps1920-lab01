package lab01.tdd.factory;

import lab01.tdd.strategy.MultipleStrategy;
import lab01.tdd.strategy.SelectStrategy;

public class MultipleFactory extends AbstractFactory {
    private int elem;

    public MultipleFactory(int i) {
        this.elem = i;
    }

    @Override
    public SelectStrategy getStrategy() {
        return new MultipleStrategy(elem);
    }
}
