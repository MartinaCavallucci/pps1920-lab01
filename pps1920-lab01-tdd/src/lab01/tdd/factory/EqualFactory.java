package lab01.tdd.factory;

import lab01.tdd.strategy.EqualStrategy;
import lab01.tdd.strategy.SelectStrategy;

public class EqualFactory extends AbstractFactory {
    private int element;

    public EqualFactory(int element) {
        this.element = element;
    }

    @Override
    public SelectStrategy getStrategy() {
        return new EqualStrategy(element);
    }
}
