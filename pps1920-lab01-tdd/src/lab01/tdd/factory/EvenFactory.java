package lab01.tdd.factory;

import lab01.tdd.strategy.EvenStrategy;
import lab01.tdd.strategy.SelectStrategy;

public class EvenFactory extends AbstractFactory {
    @Override
    public SelectStrategy getStrategy() {
        return new EvenStrategy();
    }
}
