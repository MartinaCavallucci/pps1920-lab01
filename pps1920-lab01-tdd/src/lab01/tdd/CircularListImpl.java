package lab01.tdd;
import lab01.tdd.strategy.SelectStrategy;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CircularListImpl implements CircularList {
    private List<Integer> list = new LinkedList<>();
    private Integer currentPosition = 0;

    @Override
    public void add(int element) {
        this.list.add(element);

    }
    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public boolean isEmpty() {
        return (this.list.size() == 0);
    }

    @Override
    public Optional<Integer> next() {

        if (this.list.isEmpty()) {
            return Optional.empty();
        } else if (this.currentPosition == this.list.size() - 1) {
            currentPosition = 0;
            return Optional.of(this.list.get(0));
        } else {
            Integer pos = currentPosition + 1;
            currentPosition += 1;
            return (Optional.of(this.list.get(pos)));
        }

    }

    @Override
    public Optional<Integer> previous() {
        if (this.list.isEmpty()) {
            return Optional.empty();
        } else if (this.currentPosition == 0) {
            currentPosition = this.list.size() - 1;
            return Optional.of(this.list.get(currentPosition));
        } else {
            Integer pos = currentPosition - 1;
            currentPosition -= 1;
            return (Optional.of(this.list.get(pos)));
        }
    }

    @Override
    public void reset() {
        this.list.set(currentPosition, this.list.get(0));
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        Integer pos = -1;
        if (this.list.isEmpty()) {
            return Optional.empty();
        } else {
            for (int i = currentPosition; i < list.size() + currentPosition; i++) {
                if (strategy.apply(list.get(i))) {
                    pos = i;
                    break;
                }

            }
            if(pos != -1)
                return (Optional.of(this.list.get(pos)));
            else
                return Optional.empty();
        }
    }
}



