package lab01.tdd.strategy;

import lab01.tdd.strategy.SelectStrategy;

public class MultipleStrategy implements SelectStrategy {
    private int value;

    public MultipleStrategy(int elem) {
        this.value = elem;
    }

    @Override
    public boolean apply(int element) {

        return element % this.value ==0;
    }
}
