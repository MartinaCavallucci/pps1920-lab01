package lab01.tdd.strategy;

public class EqualStrategy implements SelectStrategy {
    private int value;
    public EqualStrategy(int value) {
        this.value = value;
    }

    @Override
    public boolean apply(int element) {
        return this.value == element;
    }
}
