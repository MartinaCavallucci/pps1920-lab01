package lab01.example.model;

public interface BankAccountAtm extends BankAccount {

    public void depositAtm(int usrID, double amount);
    public void withdrawAtm(int usrID, double amount);
}
