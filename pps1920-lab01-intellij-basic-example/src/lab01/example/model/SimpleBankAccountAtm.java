package lab01.example.model;

public class SimpleBankAccountAtm extends SimpleBankAccount implements BankAccountAtm {
    public static Integer FEE = 1;
    public SimpleBankAccountAtm(AccountHolder holder, double balance) {
        super(holder, balance);
    }

    public void depositAtm(int usrID, double amount) {
        super.deposit(usrID, amount - FEE);
    }

    public void withdrawAtm(int usrID, double amount) {
        super.withdraw(usrID, amount + FEE);
    }

}
