import lab01.example.model.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the SimpleBankAccount implementation
 */
class SimpleBankAccountAtmTest {

    private AccountHolder accountHolder;
    private BankAccountAtm bankAccount;

    @BeforeEach
    void beforeEach(){
        accountHolder = new AccountHolder("Mario", "Rossi", 1);
        bankAccount = new SimpleBankAccountAtm(accountHolder, 0);
    }

    @Test
    void testInitialBalance() {
        assertEquals(0, bankAccount.getBalance());
    }

    @Test
    void testDeposit() {
        bankAccount.depositAtm(accountHolder.getId(), 100);
        assertEquals(99, bankAccount.getBalance());
    }

    @Test
    void testWrongDepositAtm() {
        bankAccount.depositAtm(accountHolder.getId(), 100);
        bankAccount.depositAtm(2, 50);
        assertEquals(99, bankAccount.getBalance());
    }

    @Test
    void testWithdrawAtm() {
        bankAccount.depositAtm(accountHolder.getId(), 100);
        bankAccount.withdrawAtm(accountHolder.getId(), 70);
        assertEquals(28, bankAccount.getBalance());
    }

    @Test
    void testWrongWithdrawAtm() {
        bankAccount.depositAtm(accountHolder.getId(), 100);
        bankAccount.withdrawAtm(2, 70);
        assertEquals(99, bankAccount.getBalance());
    }
}
